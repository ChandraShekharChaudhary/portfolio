# Portfolio


An overview of the HTML and CSS files that used to make up portfolio website. It includes information about the file structure, important components, and instructions for running the website.

### File Structure
The file structure of your portfolio website may look like this:
```
portfolio/
├── images/
│   ├── image1.jpg
│   ├── image2.jpg
├── index.html
├── style.css
└── README.md
```
* The css file contains css code that define the styles for this website.
* The images directory contains any images used on this website.
* The index.html file is the main HTML file that represents the homepage of your portfolio website.
* The README.md file is this document, providing information about the website.

### Important Components
**index.html**

The index.html file is the main entry point for the portfolio website. It contains the structure and content of homepage. 

**style.css**

The style.css file contains the CSS rules that define the visual appearance of website. This can be  modified to change colors, fonts, layout, and other styles to match your design preferences.

**images/**

The images directory contains all images that include on the website. 

**Link**

https://portfolio-iota-mocha-10.vercel.app/